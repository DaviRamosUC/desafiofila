package program;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class FilaAlunos {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {

		Queue<Aluno> alunos = new LinkedList<>();
		operacoes(alunos);

	}

	public static void operacoes(Queue<Aluno> fila) {
		Integer op=0;
		do {
			System.out.println("1) Adicionar 2) Remover 3)Quantidade 4)Listar 5)Primeiro da fila 6)Sair");
			System.out.print("Informe qual opera��o deseja fazer: ");
			op = sc.nextInt();
			sc.nextLine();
			switch (op) {
			case 1: {
				fila.add(addAluno());
				System.out.println("Aluno adicionado \n");
				break;
			}
			case 2: {
				int contador =0;
				System.out.print("Informe o nome do aluno: ");
				String nome = sc.nextLine();
				for (Aluno aluno : fila) {
					if (aluno.getNome().equals(nome)) {
						fila.remove(aluno);
					}else{
						contador++;
					}
				}
				if (contador == fila.size()) {
					System.out.println("O aluno n�o est� na lista \n");
				}else {
					System.out.println("Aluno removido  \n");					
				}
				break;
			}
			case 3: {
				System.out.println("Tamanho da fila: "+fila.size()+" \n");
				break;
			}
			case 4: {
				for (Aluno aluno : fila) {
					System.out.println(aluno.toString());
				}
				System.out.println(" \n");
				break;
			}
			case 5: {
				System.out.println(fila.peek());
				System.out.println(" \n");
				break;
			}
			case 6: {
				System.out.println("Valeu mano at� a pr�xima");
				System.exit(0);
				break;
			}
			default:
				System.out.println("Essa op��o n�o existe");
			}
		} while (op != 6);
	}

	public static Aluno addAluno() {
		System.out.print("Informe o nome do aluno: ");
		String nome = sc.nextLine();

		return new Aluno(nome);
	}

}
